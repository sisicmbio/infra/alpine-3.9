FROM alpine:3.9

RUN apk add --update  --no-cache ca-certificates  \
    tzdata \
    curl \
    openssh-client \
    libltdl \
    shadow \
    util-linux \
    pciutils \
    usbutils \
    coreutils \
    binutils \
    findutils \
    grep \
    bash \
    bash-doc \
    bash-completion && \
    rm -rf /var/lib/apt/lists/* \
    && cp  /usr/share/zoneinfo/Brazil/East /etc/localtime  \
    && echo "America/Sao_Paulo" >  /etc/timezone  \
    && apk del tzdata

RUN apk add --update --no-cache python3 && \
        python3 -m ensurepip && \
        rm -r /usr/lib/python*/ensurepip && \
        pip3 install --upgrade pip setuptools && \
        if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
        if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
        rm -r /root/.cache && \
        rm -rf /var/lib/apt/lists/*

ADD serpro-ca/root_1.crt /usr/local/share/ca-certificates/root_1.crt
ADD serpro-ca/root_2.crt /usr/local/share/ca-certificates/root_2.crt
ADD serpro-ca/root_3.crt /usr/local/share/ca-certificates/root_3.crt
ADD icmbio-ca/CA_ICMBIO.crt /usr/local/share/ca-certificates/CA_ICMBIO.crt

RUN update-ca-certificates

